/** SSD1306 OLED Library
	Communication through I2C mode
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2019 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SSD1306_CNF_H
#define SSD1306_CNF_H

/** define all interrupt levels here (0 is the highest, 31 the lowest prio) */

#define SSD1306_I2C_CH          I2C_CH_OLED      ///! SPI channel used for communication

#define SSD1306_CONTRAST_DEFAULT 0xCF            ///! contrast 0..0xff (where 0 is lowest and 0xff is highest contrast)

#define SSD1306_SIZE_X 128
#define SSD1306_SIZE_Y 32 // 64

#endif
