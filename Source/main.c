/** RxUSB
	An USB HID-Joystick interface for RC receivers

	----------------------------------------------------------
	Copyright 2019 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


#include "global.h"
#include <string.h>
#include <stdlib.h>
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "dma.h"
#include "uart.h"
#include "sct.h"
#include "systime.h"
#include "systick.h"
#include "systime.h"
#include "romapi.h"
#include "adc.h"
#include "cmsis/app_usbd_cfg.h"
#include "cmsis/hid_generic.h"
#include "segger/SEGGER_RTT.h"



#define PIN_DEBOUNCE_COUNT 10
#define NUMBER_OF_AXES 6

/* Pin definitions - this is the physical output pin for a given function */
#define PIN_BUTTON_0    PIN_CH(0,24)
#define PIN_BUTTON_1    PIN_CH(0,25)
#define PIN_BUTTON_2    PIN_CH(0,0)
#define PIN_BUTTON_4    PIN_CH(0,12)
#define PIN_BUTTON_5    PIN_CH(0,11)
#define PIN_BUTTON_6    PIN_CH(0,10)
#define PIN_BUTTON_7    PIN_CH(0,18)

#define ZERO_POS_PERIOD (SYS_FRQ_SCT*1.5/1000) //1.5ms in SCT ticks
#define RANGE_PERIOD    (SYS_FRQ_SCT*0.8/1000) //800µs in SCT ticks

static USBD_HANDLE_T g_hUsb;
const  USBD_API_T *g_pUsbApi;

#define NUM_CHANNELS 6
#define NUM_BUTTONS  7


/// general channel configuration options
#define CNF_INVERT   (1UL<<7)
#define CNF_DISABLE  0
#define CNF_ENABLE   (1UL<<0)

/// RX mode channel configuration options
#define CNF_BUTTON(x)  ((x)<<1)

#define CNF_GET_BUTTON(x) ( ((x) >> 1)&7 )

/// Joystick mode channel configuration options
#define CNF_DIGITAL (1UL<<2)
#define CNF_ANALOG  0

////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
	u8 button_mask;
	u8 channel[NUM_CHANNELS];
} config_t;

config_t config = {
	0xff,        // button mask
	{
		CNF_ENABLE, ///! X
		CNF_ENABLE, ///! Y
		CNF_ENABLE|CNF_BUTTON(1), ///! Z
		CNF_ENABLE|CNF_BUTTON(2), ///! RX
		CNF_DISABLE, ///! RY
		CNF_DISABLE, ///! RZ
	}
};

////////////////////////////////////////////////////////////////////////////////////////

u8 adc_channels[] = { ADC_CH_V0, ADC_CH_V1, ADC_CH_V2, ADC_CH_V3, ADC_CH_V4, ADC_CH_V5 };

u8 hid_output_report[HID_OUT_REPORT_SIZE];

// input debouncing
typedef struct {
	u8 pin;          ///! pin number
	s8 ctr;          ///! debounce counter
	u8 state;        ///! debounce state: 0: debounced, 1: debouncing ongoing
	volatile u8 lvl; ///! debounced logic level
	u8 inact_lvl;    ///! inactive level: 0: low, 1: high
} pin_debounce_t;

/** debounce structure for the input pins */
pin_debounce_t pins[] = {
	/* pin, counter, state, level, inactive level */
	{PIN_BUTTON_0, 0,0,0,1},
	{PIN_BUTTON_1, 0,0,0,1},
	{PIN_BUTTON_2, 0,0,0,1},
	{PIN_BUTTON_4, 0,0,0,1},
	{PIN_BUTTON_5, 0,0,0,1},
	{PIN_BUTTON_6, 0,0,0,1},
	{PIN_BUTTON_7, 0,0,0,1},
};

u32 pin_mask;

#define BUTTON_0 0
#define BUTTON_1 1
#define BUTTON_2 2
#define BUTTON_4 3
#define BUTTON_5 4
#define BUTTON_6 5
#define BUTTON_7 6

char str_buffer[64];
char str_buffer_bgnd[64];

volatile u32 ms_counter;

/** UART receive buffer */
char uart_rx_buf[64];
/** UART number of received bytes */
volatile u16 uart_rx_len;

u32 random() {
	return SCT_GetCounter(LPC_SCT0) ^ PRC_ReverseByteOrder(SCT_GetCounter(LPC_SCT1));
}

u8 pin_level_raw(u8 ch) {
	pin_debounce_t *p = &pins[ch];
	return PIN_ChGet(p->pin) ^ p->inact_lvl;
}

u8 pin_debounce(u8 ch) {
	u8 changed = 0;
	pin_debounce_t *p = &pins[ch];
	u8 lvl = PIN_ChGet(p->pin) ^ p->inact_lvl;
	if (p->state == 0) {
		// not yet debouncing
		if (lvl != p->lvl) {
			// state change -> start debouncing
			p->ctr = PIN_DEBOUNCE_COUNT;
			p->state = 1;
		}
	} else {
		// already debouncing
		if (lvl != p->lvl) {
			// current level is different than the debounced level
			if (--p->ctr <= 0) {
				// new level was debounced
				p->state = 0;
				p->lvl = lvl;
				changed = 1;
			}
		} else {
			// current level is the same as the debounced level
			if (++p->ctr >= PIN_DEBOUNCE_COUNT) {
				// reset debounce state
				p->state = 0;
			}
		}
	}
	return changed;
}

u8 pin_state(u8 ch) {
	return pins[ch].lvl;
}


/** Convert integer to string.
	@param z integer value to convert
	@param buffer buffer of char to write to (must be large enough to hold string)
*/
void int2str( int z, char* buffer ) {
	int i = 0;
	int j;
	char tmp;
	unsigned u;

	if( z < 0 ) {
		buffer[0] = '-';
		buffer++;
		u = -z;
	} else
		u = (unsigned)z;
	do {
		buffer[i++] = '0' + u % 10;
		u /= 10;
	} while( u > 0 );

	for( j = 0; j < i / 2; ++j ) {
		tmp = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = tmp;
	}
	buffer[i] = 0;
}

/** Convert integer to string.
	@param u unsigned integer value to convert
	@param buffer buffer of char to write to (must be large enough to hold string)
*/
void int2hex( u32 u, char* buffer ) {
	static const char hexchar[] = "0123456789abcdef";
	int i = 0;
	int j;
	char tmp;

	do {
		buffer[i++] = hexchar[u % 16];
		u /= 16;
	} while( u > 0 );

	for( j = 0; j < i / 2; ++j ) {
		tmp = buffer[j];
		buffer[j] = buffer[i-j-1];
		buffer[i-j-1] = tmp;
	}
	buffer[i] = 0;
}

char* timestring(u32 t, char *b) {
	int h,m,s,pos;
	h   = t / 3600;
	m = (t / 60) % 60;
	s = t % 60;
	// hour
	if (h<10) {
		b[0] = '0';
		pos = 1;
	} else pos = 0;
	int2str(h,&b[pos]);
	pos = strlen(b);
	b[pos++]= ':';
	// minute
	if (m<10) {
		b[pos++] = '0';
	}
	int2str(m,&b[pos]);
	pos = strlen(b);
	b[pos++]= '.';
	// second
	if (s<10) {
		b[pos++] = '0';
	}
	int2str(s,&b[pos]);
	return b;
}

/** Append space to the end of a string until it reaches a certain length.
	@param s null terminated string to append to
	@param l length to reach
 */
void str_pad_right(char* s, int l) {
	int len = strlen(s);
	while (len<l) {
		s[len]=' ';
		s[len+1]=0;
		len = strlen(s);
	}
}

/** Append space to the start of a string until it reaches a certain length.
	@param s null terminated string to append to
	@param l length to reach
 */
void str_pad_left(char* s, int l) {
	int i;
	int len = strlen(s);

	if (l > len) {
		// shift string right
		for (i=0; i<=len; i++) {
			s[l-i] = s[len-i];
		}
		// fill start with spaces
		for (i=0; i<l-len; i++)
			s[i] = ' ';
	}
}

/** 1ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1ms(u32 dummy) {
	(void)dummy;
	int i;
	u32 mask=1;
	for (i=sizeof(pins)/sizeof(pins[0])-1; i>=0; i--) {
		pin_debounce(i);
		mask <<= 1;
		mask |= pin_state(i);
	}
	pin_mask = mask;

}

/*__INLINE*/ u8 convert_voltage(u16 ch) {
	return (ADC_GetResult(0,ch)>>4)-128;
}

/* __INLINE*/ u8 convert_period(u32 ticks) {
	if (ticks == 0)
		return 0;
	s32 retval = (ticks-ZERO_POS_PERIOD)/(RANGE_PERIOD/255);
	if (retval < -127)
		retval = -127;
	else if (retval > 127)
		retval = 127;
	return retval;
}

/** 10ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_10ms(u32 dummy) {
	(void)dummy;
	int i,cnf;
	s8 temp;
	u8 buttons = 0;

	for (i=0; i < NUM_CHANNELS; i++) {
		cnf = config.channel[i];
		if (cnf & CNF_ENABLE) {
			switch (i) {
				case 0:
					temp = convert_period(SCT_GetDualCaptureTicks(LPC_SCT0, 0));
					break;
				case 1:
					temp = convert_period(SCT_GetDualCaptureTicks(LPC_SCT1, 0));
					break;
				case 2:
					temp = convert_period(SCT_GetDualCaptureTicks(LPC_SCT2, 0));
					break;
				case 3:
					temp = convert_period(SCT_GetDualCaptureTicks(LPC_SCT3, 0));
					break;
				case 4:
				case 5:
					temp = 0; // ACMP capture to be implemented
					break;
			}
			if (cnf & CNF_INVERT)
				temp = -temp;
			// use this channel as button?
			cnf = CNF_GET_BUTTON(cnf);
			if (cnf == 0)
				hid_data[i] = (u8)temp; // no button
			else {
				cnf = 1 << (cnf-1); // bitmask for button
				if (temp > 0)
					buttons |= cnf;
				else
					buttons &= ~cnf;
				hid_data[i] = 0;
			}
		} else {
				hid_data[i] = 0;
		}
	}
	hid_data[6] = buttons & config.button_mask;
	adc_start_conversion(0, ADC_SEQA, ADC0_CHANNELS);
}

/** 100ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_100ms(u32 dummy) {
	(void)dummy;
}


/** 1000ms low priority task
	@param dummy unused parameter needed for Prio_Wrap_Func_t
*/
void task_1000ms(u32 dummy) {
	int2str(ms_counter/1000, str_buffer);
	SEGGER_RTT_Write(0, str_buffer, strlen(str_buffer));
	SEGGER_RTT_Write(0, "\r\n", 2);
	(void)dummy;

}

void SysTick_Handler(void) {
	PrioWrap_Function(task_1ms, 0);
	if (ms_counter % 10 == 0)
		PrioWrap_Function(task_10ms, 0);
	if (ms_counter % 100 == 0)
		PrioWrap_Function(task_100ms, 0);
	if (ms_counter % 1000 == 0)
		PrioWrap_Function(task_1000ms, 0);

	ms_counter++;
}

/**
 * @brief	Handle interrupt from USB
 * @return	Nothing
 */
void USB_IRQHandler(void) {
	USBD_API->hw->ISR(g_hUsb);
}

int main(void) {
	SystemInit();

	SEGGER_RTT_Init();

	adc_init();
	adc_start_calibration();

	SYSTIME_Init();                         /* init system timer */
	PIN_Init();

	DMA_Init();
	DMA_IRQEnable(1);

	// Init UART
	UART_SetBaudRate(UART_LOGCTRL, 115200);
	UART_Setup(UART_LOGCTRL);

	// I2C_Init();
	// SPI_Init();

	USBD_API_INIT_PARAM_T usb_param;
	USB_CORE_DESCS_T desc;
	ErrorCode_t ret = LPC_OK;
	/* initialize USBD ROM API pointer. */
	g_pUsbApi = (const USBD_API_T *) LPC_ROM_API->pUSBD;

	/* power UP USB Phy */
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_USBPHY_PD);
	/* Reset USB block */
	//Chip_SYSCTL_PeriphReset(RESET_USB);

	/* initialize call back structures */
	memset((void *) &usb_param, 0, sizeof(USBD_API_INIT_PARAM_T));
	usb_param.usb_reg_base = LPC_USB0_BASE;
	/*	WORKAROUND for artf44835 ROM driver BUG:
	    Code clearing STALL bits in endpoint reset routine corrupts memory area
	    next to the endpoint control data. For example When EP0, EP1_IN, EP1_OUT,
	    EP2_IN are used we need to specify 3 here. But as a workaround for this
	    issue specify 4. So that extra EPs control structure acts as padding buffer
	    to avoid data corruption. Corruption of padding memory doesn’t affect the
	    stack/program behaviour.
	 */
	usb_param.max_num_ep = 2 + 1;
	usb_param.mem_base = USB_STACK_MEM_BASE;
	usb_param.mem_size = USB_STACK_MEM_SIZE;

	/* Set the USB descriptors */
	desc.device_desc = (uint8_t *) USB_DeviceDescriptor;
	desc.string_desc = (uint8_t *) USB_StringDescriptor;

	/* Note, to pass USBCV test full-speed only devices should have both
	 * descriptor arrays point to same location and device_qualifier set
	 * to 0.
	 */
	desc.high_speed_desc = USB_FsConfigDescriptor;
	desc.full_speed_desc = USB_FsConfigDescriptor;
	desc.device_qualifier = 0;

	while (!adc_calibration_finished(0) /*|| !adc_calibration_finished(1)*/); // needs about 290µs
	adc_start_conversion(0, ADC_SEQA, ADC0_CHANNELS);
//	adc_start_conversion(1, ADC_SEQA, ADC1_CHANNELS);
	/* USB Initialization */
	ret = USBD_API->hw->Init(&g_hUsb, &desc, &usb_param);
	if (ret == LPC_OK) {
		ret = usb_hid_init(g_hUsb, (USB_INTERFACE_DESCRIPTOR *) &USB_FsConfigDescriptor[sizeof(USB_CONFIGURATION_DESCRIPTOR)],
			&usb_param.mem_base, &usb_param.mem_size);
		if (ret == LPC_OK) {
			/*  enable USB interrupts */
			NVIC_EnableIRQ(USB0_IRQn);
			/* now connect */
			USBD_API->hw->Connect(g_hUsb, 1);
		}

	}

	SCT_SetInmux(LPC_SCT0, 0, 2); /* SCT0_IN0 set to P0.17 */
	SCT_PWM_SetupDualCapture(LPC_SCT0, 0 /*c*/, 0 /*in*/, 1 /*pol*/);
	SCT_Start(LPC_SCT0);

	SCT_SetInmux(LPC_SCT1, 0, 0); /* SCT1_IN0 set to P0.15 */
	SCT_PWM_SetupDualCapture(LPC_SCT1, 0 /*c*/, 0 /*in*/, 1 /*pol*/);
	SCT_Start(LPC_SCT1);

	SCT_SetInmux(LPC_SCT2, 0, 1); /* SCT1_IN0 set to P0.27 */
	SCT_PWM_SetupDualCapture(LPC_SCT2, 0 /*c*/, 0 /*in*/, 1 /*pol*/);
	SCT_Start(LPC_SCT2);

	SCT_SetInmux(LPC_SCT3, 0, 1); /* SCT1_IN0 set to P0.7 */
	SCT_PWM_SetupDualCapture(LPC_SCT3, 0 /*c*/, 0 /*in*/, 1 /*pol*/);
	SCT_Start(LPC_SCT3);
	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	PrioWrap_Init();
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 1ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

	//HID_SetFeatureReport(&config,sizeof(config_t));

	while (1) {
		/* Handle HID tasks */
		HID_Tasks();
		if (HID_OutputReportReceived()) {
			PRC_Int_Disable();
			HID_GetOutputReport(hid_output_report, sizeof(hid_output_report));
			SEGGER_RTT_Write(0, (char*)hid_output_report, sizeof(hid_output_report));
			SEGGER_RTT_Write(0, "\r\n", 2);
			PRC_Int_Enable();
		}
		/* Sleep until next IRQ happens */
		__WFI();
	}
}
