/** Math library
	Collection of filters, interpolation, characteristics
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef MATH_C
#define MATH_H

#ifndef __INLINE
#include "global.h"
#endif

#define MATH_TRI_SCALE      0x4000                       ///! Scaling factor for sine table: 0x4000 == 1.0
#define MATH_TRI_ANG_2PI_LD 30                           ///! 2*PI is 1<<MATH_TRI_ANG_2PI_LD
#define MATH_TRI_ANG_2PI    (1<<MATH_TRI_ANG_2PI_LD)     ///! 360° or 2*PI
#define MATH_TRI_ANG_PI     (MATH_TRI_ANG_2PI/2)         ///! 180° or PI

/** Moving average filter U16 */
typedef struct {
	u16 *data; ///! pointer to ring buffer
	u8  size;  ///! size of buffer
	u8  index; ///! buffer index
} flt_mov_avgu16_t;

/** Moving average filter S16 */
typedef struct {
	s16 *data; ///! pointer to ring buffer
	u8  size;  ///! size of buffer
	u8  index; ///! buffer index
} flt_mov_avgs16_t;

/** Cosine function.
	@param x angle in degree (e.g. 90°)
	@return cos(x)
*/
#define MATH_Cosine(x) (MATH_Sine((x) + 90))

/** Calculate absolute value
	@param i value to calculate ABS(i) from
	@param asolute value of i: abs(i)
*/
#define ABS(i) ((i>=0)?(i):-(i))


/** Initialize moving average filter for u16 values
	@param flt filter structure
	@param d   pointer to data buffer
	@param s   size of data buffer
 */
#define FLT_AvgU16_Init(flt, d, s) { flt.data=(d); flt.size=(s); flt.index = 0; }

/** Initialize moving average filter for s16 values
	@param flt filter structure
	@param d   pointer to data buffer
	@param s   size of data buffer
 */
#define FLT_AvgS16_Init(flt, d, s) FLT_AvgU16_Init((flt), (d), (s))

/** Add new u16 value to moving average filter
	@param flt filter structure
	@param value u16 value to add
 */
#define FLT_AvgU16_Set(flt, value) { flt.data[flt.index]=(value); if (++flt.index >= flt.size) flt.index=0;  }

/** Add new s16 value to moving average filter
	@param flt filter structure
	@param value s16 value to add
 */
#define FLT_AvgS16_Set(flt, value) FLT_AvgU16_Set((flt), (value))

/** Interpolate between two sample points.
	@param x x value
	@param x0 x value of left sample point
	@param x1 x value of right sample point
	@param y0 y value of left sample point
	@param y1 y value of right sample point
	@return interpolated y value
 */
static __INLINE s32 MAP_S32_Interpol(s32 x, s32 x0, s32 x1, s32 y0, s32 y1) {
	return y0 + (((x-x0) * (y1-y0))/(x1-x0));
}

/* Prototypes */

extern s32 MAP_S32_GetValue(u32 value, u8 size, s32 *axis, s32 *values);
extern u16 FLT_AvgU16_Get(flt_mov_avgu16_t flt);
extern s16 FLT_AvgS16_Get(flt_mov_avgs16_t flt);
extern s32 MATH_Sine(s32 x);
extern s32 Math_Atan2(s32 y, s32 x);
extern u32 Math_Sqrt(u32 n);

#endif
