/** ADC Library
	Definitions for calling IAP ROM functions
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef ADC_H
#define ADC_H

#include "adc_cnf.h"

extern LPC_ADC_T *adc[];

#define ADC_SEQA 0
#define ADC_SEQB 1

//#define LPC_ADC LPC_ADC0 // workaround until ADC1 support is added

#define LPC_ADC_SEQ_CTRL(dev, x)  ((dev)->SEQ_CTRL[x])
#define LPC_ADC_SEQ_GDAT(dev, x)  ((dev)->SEQ_GDAT[x])
#define LPC_ADC_SEQ_THR_LO(dev,x) ((dev)->THR_LOW[x])
#define LPC_ADC_SEQ_THR_HI(dev,x) ((dev)->THR_HIGH[x])

#if ADC_ASYNC_MODE
#define SYS_CLK_ADC_FRQ SYS_FRQ_ASYNC_ADC
#else
#define SYS_CLK_ADC_FRQ SYS_FRQ_CORE
#endif

/* ADC Control register CTRL */

#define ADC_CTRL_CLKDIV(x)        (((x)-1) & 0xff)     	    ///! System clock divider (0..255)
#define ADC_CTRL_CLKDIV_MASK      0xff
#define ADC_CTRL_CLKDIV_FREQ(f)   ((SYS_CLK_ADC_FRQ/f)-1)
#define ADC_CTRL_ASYNMODE         (1UL <<  8)               ///! Select Asynchronous mode.
#define ADC_CTRL_MODE10BIT        (1UL <<  9)               ///! 10-bit conversion.
#define ADC_CTRL_LPWRMODE         (1UL << 10)               ///! Select low-power ADC mode.
#define ADC_CTRL_CALMODE          (1UL << 30)               ///! Initiate self-calibration cycle

/** Read ADC control register.
	@return value of control register
*/
#define ADC_GetCtrl(dev)   ((dev)->CTRL)

/** Set ADC control register.
	@param x value to set - use the ADC_CTRL_xxx macros
*/
#define ADC_SetCtrl(dev,x)   {(dev)->CTRL = (u32)(x);}


static inline u8 adc_calibration_finished(u8 dev) {
	return (u8) ((adc[dev]->CTRL & ADC_CTRL_CALMODE) == 0);
}


/* ADC Sequencer Control register SEQ_CTRL */

#define ADC_SEQCTRL_CHANNEL(x)    (1UL << (x))              ///! Channel select macro
#define ADC_SEQCTRL_TRIGGER_SW           0                  ///! Software trigger
#define ADC_SEQCTRL_TRIGGER_ADC_PINTRG0  (1UL<<12)          ///! Hardware trigger source PINTRIG0
#define ADC_SEQCTRL_TRIGGER_ADC_PINTRIG1 (2UL<<12)          ///! Hardware trigger source PINTRIG1
#define ADC_SEQCTRL_TRIGGER_SCT0_OUT3    (3UL<<12)          ///! Hardware trigger source SCT_OUT3
#define ADC_SEQCTRL_TRIGGER_ACMP_O       (4UL<<12)          ///! Hardware trigger source ACMP_O
#define ADC_SEQCTRL_TRIGGER_ARM_TXEV     (5UL<<12)          ///! Hardware trigger source core TXEV
#define ADC_SEQCTRL_TRIGPOL_FALLING      0                  ///! HW trigger on falling edge
#define ADC_SEQCTRL_TRIGPOL_RISING       (1UL<<18)          ///! HW trigger on rising edge
#define ADC_SEQCTRL_SYNCBYPASS           (1UL << 19)        ///! Bypass synchronization flip-flops */
#define ADC_SEQCTRL_START                (1UL << 26)        ///! Start conversion */
#define ADC_SEQCTRL_BURST                (1UL << 27)        ///! Repeatedly cycle through sequence */
#define ADC_SEQCTRL_SINGLESTEP           (1UL << 28)        ///! Single step sequence */
#define ADC_SEQCTRL_MODE_END_OF_CONV     0                  ///! Mode: end of conversion */
#define ADC_SEQCTRL_MODE_END_OF_SEQ      (1UL << 30)        ///! Mode: end of sequence */
#define ADC_SEQCTRL_SEQ_ENA              (1UL << 31)        ///! Sequence enable bit */

/** Read ADC sequencer control register.
    @param sequence (A=0, B=1)
	@return value of sequencer control register
*/
#define ADC_GetSeqCtrl(dev, s)   (LPC_ADC_SEQ_CTRL(dev, s))

/** Set ADC sequencer control register.
    @param sequence (A=0, B=1)
	@param x value to set - use the ADC_SEQCTRL_xxx macros
*/
#define ADC_SetSeqCtrl(dev, s,x)   {LPC_ADC_SEQ_CTRL(dev, s) = (u32)(x);}


/* ADC global data register */
#define ADC_SEQGDAT_RESULT(x)           (((x)>>4)&0xfff)
#define ADC_SEQGDAT_THCMPRANGE(x)       (((x)>>16)&3)
#define ADC_SEQGDAT_THCMPCROSS(x)       (((x)>>18)&3)
#define ADC_SEQGDAT_CHANNEL(x)          (((x)>>26)&0xf)
#define ADC_SEQGDAT_OVERRUN(x)          (((x)>>30)&1)
#define ADC_SEQGDAT_DATAVALID(x)        (((x)>>31)&1)

/** Read ADC global data register.
    @param sequence (A=0, B=1)
	@return value of global data register
*/
#define ADC_GetSeqData(s)   (LPC_ADC_SEQ_GDAT(s))


/* ADC (channel) data registers */
#define ADC_THCMPRANGE_INRANGE     0
#define ADC_THCMPRANGE_BELOW       2
#define ADC_THCMPRANGE_ABOVE       3

#define ADC_THCMPCROSS_NOCROSS     0
#define ADC_THCMPCROSS_DOWNWARD    2
#define ADC_THCMPCROSS_UPWAR       3


#define ADC_DAT_RESULT(x)           (((x)>>4)&0xfff)
#define ADC_DAT_THCMPRANGE(x)       (((x)>>16)&3)
#define ADC_DAT_THCMPCROSS(x)       (((x)>>18)&3)
#define ADC_DAT_CHANNEL(x)          (((x)>>26)&0xf)
#define ADC_DAT_OVERRUN(x)          (((x)>>30)&1)
#define ADC_DAT_DATAVALID(x)        (((x)>>31)&1)

/** Read ADC channel data register.
    @param channel (0..11)
	@return value of channel data register
*/
#define ADC_GetData(dev,ch)     ((dev)->DAT[ch])

/** Read 12bit ADC value from ADC channel data register.
    @param channel (0..11)
	@return 12bit ADC value
*/
#define ADC_GetResult(dev,ch)   (ADC_DAT_RESULT((adc[dev])->DR[ch]))


/** ADC low/high threshold register */

/** Read ADC low threshold.
    @param threshold index (0..1)
	@return 12bit low threshold
*/
#define ADC_GetThrLow(dev,i)   ((LPC_ADC_SEQ_THR_LO(dev,i)>>4)&0xfff)

/** Set ADC low threshold.
    @param threshold index (0..1)
	@param x 12bit threshold to set
*/
#define ADC_SetThrLow(dev,i,x)   {LPC_ADC_SEQ_THR_LO(dev,i) = ((u32)(x)&0xfff)<<4;}

/** Read ADC high threshold.
    @param threshold index (0..1)
	@return 12bit high threshold
*/
#define ADC_GetThrHigh(dev,i)   ((LPC_ADC_SEQ_THR_HI(dev,i)>>4)&0xfff)

/** Set ADC high threshold.
    @param threshold index (0..1)
	@param x 12bit threshold to set
*/
#define ADC_SetThrHigh(dev,i,x)   {LPC_ADC_SEQ_THR_HI(dev,i) = ((u32)(x)&0xfff)<<4;}

#define ADC_THRSEL_0(ch) 0
#define ADC_THRSEL_1(ch) (1UL<<(ch))

/** Read ADC threshold select register.
	@return value of threshold select register
*/
#define ADC_GetThrSel(dev) ((dev)->CHAN_THRSEL)

/** Set ADC threshold select register.
	@param x value to write to register (construct from ADC_THRSEL_xxx macros)
*/
#define ADC_SetThrSel(dev,x) {(dev)->CHAN_THRSEL = (x);}


/* ADC Interrupt Enable register */
#define ADC_INTEN_SEQ(s)            (1UL << ((s)&1))   ///! Sequence A/B Interrupt enable */
#define ADC_INTEN_OVRRUN            (1UL << 2)         ///! Overrun Interrupt enable bit */
#define ADC_INTEN_CMP_OUTSIDETHR(x) (1UL << (3+2*(x))) ///! Outside threshold interrupt x=0..11 */
#define ADC_INTEN_CMP_CROSSTH(x)    (2UL << (3+2*(x))) ///! Crossing threshold interrupt x=0..11 */

/** Read ADC interrupt enable register.
	@return value of interrupt enable register
*/
#define ADC_GetIntEnable(dev) ((dev)->INTEN)

/** Set ADC interrupt enable register.
	@param x value to write to register (construct from ADC_INTEN_xxx macros)
*/
#define ADC_SetIntEnable(dev,x) {(dev)->INTEN = (x);}


/* ADC Flags register bit */
#define ADC_FLAGS_THCMP(ch)            (1UL << (ch))         ///! Threshold comparison event for channel ch */
#define ADC_FLAGS_OVRRUN(ch)           (1UL << (12+(ch)))    ///! Overrun status for channel ch */
#define ADC_FLAGS_SEQ_OVRRUN_MASK(seq) (1UL << (24+(seq)))   ///! Seq A/B Overrun status */
#define ADC_FLAGS_SEQN_INT(seq)        (1UL << (28+(seq)))   ///! Seq A/B Interrupt/DMA status */
#define ADC_FLAGS_THCMP_INT            (1UL << 30)           ///! Threshold comparison Interrupt/DMA status */
#define ADC_FLAGS_OVRRUN_INT           (1UL << 31)           ///! Overrun Interrupt status */

/** Read ADC flags register.
	@return value of flags register
*/
#define ADC_GetFlags(dev) ((dev)->FLAGS)

/** Clr ADC flag register.
	@param x bitmask of values to clear (construct from ADC_FLAGS_xxx macros)
*/
#define ADC_SetIntEnable(dev,x) {(dev)->INTEN = (x);}


/* ADC Trim register */

#define ADC_TRIM_VRANGE_HIGH 0            ///! 2.7V to 3.6V
#define ADC_TRIM_VRANGE_LOW  (1UL<<5)     ///! 2.4V to 2.7V

/** Read ADC rim register.
	@return value of trim register
*/
#define ADC_GetTrimRange(dev) ((dev)->TRM)

/** Clr ADC trim register.
	@param x trim voltage range (use ADC_TRIM_xxx macros)
*/

#define ADC_SetTrim(dev,x) {(dev)->TRM = (x);}

extern void adc_init(void);
extern void adc_shutdown(void);
extern void adc_start_calibration(void);
extern void adc_start_conversion(u8 dev, u8 sequence, u32 channels);

#endif
