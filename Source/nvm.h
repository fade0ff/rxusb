/** NVM library
	Non volatile memory implemented via an I2C EEPROM
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef NVM_H
#define NVM_H

#include "nvm_cnf.h"

/* Prototypes */
extern void NVM_Init(void);
extern u8 NVM_Write(u16 adr, u8 *data, u8 len);
extern u8 NVM_Read(u16 adr, u8 *data, u8 len);
extern void NVM_WaitForCompletion(void);

#endif
