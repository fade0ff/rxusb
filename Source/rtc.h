/** RTC library
	Use of Real Time clock
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2016 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef RTC_H
#define RTC_H

#define RTC_CTRL_SWRESET        (1UL << 0)  ///! Apply reset to RTC
#define RTC_CTRL_OFD            (1UL << 1)  ///! Oscillator fail detect status (failed bit)
#define RTC_CTRL_ALARM1HZ       (1UL << 2)  ///! RTC 1 Hz timer alarm flag status (match) bit
#define RTC_CTRL_WAKE1KHZ       (1UL << 3)  ///! RTC 1 kHz timer wake-up flag status (timeout) bit
#define RTC_CTRL_ALARMDPD_EN    (1UL << 4)  ///! RTC 1 Hz timer alarm for Deep power-down enable bit
#define RTC_CTRL_WAKEDPD_EN     (1UL << 5)  ///! RTC 1 kHz timer wake-up for Deep power-down enable bit
#define RTC_CTRL_RTC1KHZ_EN     (1UL << 6)  ///! RTC 1 kHz clock enable bit */
#define RTC_CTRL_RTC_EN         (1UL << 7)  ///! RTC enable bit */


#define RTC_GetControl() (LPC_RTC->CTRL)

#define RTC_SetControl(x) {LPC_RTC->CTRL = (x);}

#define RTC_EnableOscCtrl(x) {LPC_SYSCTL->RTCOSCCTRL  = (x)&1;}


#define RTC_GetCount() (LPC_RTC->COUNT)

#define RTC_SetCount(x) {LPC_RTC->COUNT = (x);}


#endif
