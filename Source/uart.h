/** UART library
	Communication via asynchronous serial interface (UART)
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef UART_H
#define UART_H

#include "uart_cnf.h"

#define LPC_UART_Type LPC_USART_T

#define UART_NOSCAN 0xffff                       ///! don't use scan character


#define UART_NUM_PHYS_DEVICES  3                 ///! Number of physical devices

/* CFG */

#define UART_CFG_ENABLE         (1UL << 0)       ///! UART enable
#define UART_CFG_DATALEN_7      0                ///! 7 bit data length
#define UART_CFG_DATALEN_8      (1UL << 2)       ///! 8 bit data length
#define UART_CFG_DATALEN_9      (2UL << 2)       ///! 9 bit data length
#define UART_CFG_PARITYSEL_NONE (0UL << 4)       ///! No parity
#define UART_CFG_PARITYSEL_EVEN (2UL << 4)       ///! Even parity
#define UART_CFG_PARITYSEL_ODD  (3UL << 4)       ///! Odd parity
#define UART_CFG_STOPLEN_1      0                ///! 1 stop bit
#define UART_CFG_STOPLEN_2      (1UL << 6)       ///! 2 stop bits
#define UART_MODE32K            (1UL << 7)       ///! Use 32 kHz clock from the RTC oscillator as the clock source to the BRG (0: standard clocking )
#define UART_CFG_CTSEN          (1UL << 9)       ///! CTS enable (flow control)
#define UART_CFG_SYNCEN         (1UL << 11)      ///! Synchronous mode enable (0: asynchronous)
#define UART_CFG_CLKPOL_FALLING 0                ///! Clock polarity in synchronous mode: Un_RXD sampled on falling edge of SCLK
#define UART_CFG_CLKPOL_RISING  (1UL << 12)      ///! Clock polarity in synchronous mode: Un_RXD sampled on rising edge of SCLK
#define UART_CFG_SYNCMST_SLAVE  0                ///! Synchronous mode Master select: Slave
#define UART_CFG_SYNCMST_MASTER (1UL << 14)      ///! Synchronous mode Master select: Master
#define UART_CFG_LOOP           (1UL << 15)      ///! Enable loopback mode
#define UART_CFG_OETA           (1UL << 18)      ///! Output Enable Turnaround time enable for RS-485  operation (OESEL=1)
#define UART_CFG_AUTOADDR       (1UL << 19)      ///! Automatic Address matching enable
#define UART_CFG_OESEL          (1UL << 20)      ///! Output enable select: RTS signal configured to provide output enable signal to control RS-485 transceiver
#define UART_CFG_OEPOL_LOW      0                ///! Output enable polarity: active low (OESEL=1)
#define UART_CFG_OEPOL_HIGH     (1UL << 21)      ///! Output enable polarity: active high (OESEL=1)
#define UART_CFG_RXPOL_INV      (1UL << 22)      ///! Use inverted RX signal: rest value 0, start bit 1, data inverted, stop bit 0 (0: normal)
#define UART_CFG_TXPOL_INV      (1UL << 23)      ///! Use inverted TX signal: rest value 0, start bit 1, data inverted, stop bit 0 (0: normal)

/** Get UART configuration register.
	@param x pointer to UART peripheral
	@return configuration register CFG (see UART_CFG_xxx macros)
 */
#define UART_GetConfig(x)    ( (x)->CFG )

/** Set UART configuration register.
	@param x pointer to UART peripheral
	@param y value to write to configuration register CFG (construct from UART_CFG_xxx macros)
 */
#define UART_SetConfig(x,y)  { (x)->CFG = (u32)(y); }


/* CTRL */

#define UART_CTRL_TXBRKEN       (1UL << 1)       ///! Continuous break enable (0: off)
#define UART_CTRL_ADDRDET       (1UL << 2)       ///! Address detect mode enable (0: off)
#define UART_CTRL_TXDIS         (1UL << 6)       ///! Transmit disable (0: normal)
#define UART_CTRL_CC            (1UL << 8)       ///! Continuous clock generation in synchronous mode (0: off)
#define UART_CTRL_CLRCCONRX     (1UL << 9)       ///! Clear continuous clock (CC) automatically (0: off)
#define UART_CTRL_AUTOBAUD      (1UL << 16)      ///! Automatic baudrate enable (0: off)

/** Get UART control register.
	@param x pointer to UART peripheral
	@return control register CTRL (see UART_CTRL_xxx macros)
 */
#define UART_GetControl(x)    ( (x)->CTRL )

/** Set UART control register.
	@param x pointer to UART peripheral
	@param y value to write to control register CTRL (construct from UART_CTRL_xxx macros)
 */
#define UART_SetControl(x,y)  { (x)->CTRL = (u32)(y); }


/* STAT */

#define UART_STAT_RXRDY         (1UL << 0)       ///! Receiver ready
#define UART_STAT_RXIDLE        (1UL << 1)       ///! Receiver idle
#define UART_STAT_TXRDY         (1UL << 2)       ///! Transmitter ready
#define UART_STAT_TXIDLE        (1UL << 3)       ///! Transmitter idle
#define UART_STAT_CTS           (1UL << 4)       ///! State of CTS signal
#define UART_STAT_DELTACTS      (1UL << 5)       ///! Change in CTS state detected
#define UART_STAT_TXDISSTAT     (1UL << 6)       ///! Transmitter disabled state
#define UART_STAT_OVERRUNINT    (1UL << 8)       ///! Overrun error interrupt flag
#define UART_STAT_RXBRK         (1UL << 10)      ///! Received break (read only - cleared when the Un_RXD pin goes high)
#define UART_STAT_DELTARXBRK    (1UL << 11)      ///! Change in receive break detection state
#define UART_STAT_START         (1UL << 12)      ///! Start detected
#define UART_STAT_FRAMERRINT    (1UL << 13)      ///! Framing error interrupt flag
#define UART_STAT_PARITYERRINT  (1UL << 14)      ///! Parity error interrupt flag
#define UART_STAT_RXNOISEINT    (1UL << 15)      ///! Received noise interrupt flag
#define UART_STAT_ABERR         (1UL << 16)      ///! Autobaud error

/** Get UART status register.
	@param x pointer to UART peripheral
	@return status register STAT (see UART_STAT_xxx macros)
 */
#define UART_GetStatus(x)    ( (x)->STAT )

/** Clr bits in UART status register.
	@param x pointer to UART peripheral
	@param y bitmask of bits to clear in status register STAT (construct from UART_STAT_xxx macros)
 */
#define UART_ClrStatus(x,y)  { (x)->STAT = (u32)(y); }


/* INTENSET/INTENCLR */

#define UART_INTEN_RXRDY        (1UL << 0)       ///! Receiver ready interrupt
#define UART_INTEN_TXRDY        (1UL << 2)       ///! Transmitter ready interrupt
#define UART_INTEN_TXIDLE       (1UL << 3)       ///! Transmitter idle interrupt
#define UART_INTEN_DELTACTS     (1UL << 5)       ///! Change in CTS state interrupt
#define UART_INTEN_TXDIS        (1UL << 6)       ///! Transmitter disable interrupt
#define UART_INTEN_OVERRUN      (1UL << 8)       ///! Overrun error interrupt
#define UART_INTEN_DELTARXBRK   (1UL << 11)      ///! Change in receiver break detection interrupt
#define UART_INTEN_START        (1UL << 12)      ///! Start detect interrupt
#define UART_INTEN_FRAMERR      (1UL << 13)      ///! Frame error interrupt
#define UART_INTEN_PARITYERR    (1UL << 14)      ///! Parity error interrupt
#define UART_INTEN_RXNOISE      (1UL << 15)      ///! Received noise interrupt
#define UART_INTEN_ABERR        (1UL << 16)      ///! Auto baudrate error interrupt

/** Get UART interrupt enable register.
	@param x pointer to UART peripheral
	@return UART interrupt enable register INTENSET (see UART_INTEN_xxx macros)
 */
#define UART_GetIntEnable(x)   ( (x)->INTENSET )

/** Set UART interrupt enable register.
	@param x pointer to UART peripheral
	@param y value to write to UART interrupt enable register INTENSET (construct from UART_INTEN_xxx macros)
 */
#define UART_SetIntEnable(x,y) { (x)->INTENSET = (u32)(y);}

/** Clear bits in UART interrupt enable register.
	@param x pointer to UART peripheral
	@param y value to write to UART interrupt clear register INTENCLR (construct from UART_INTEN_xxx macros)
 */
#define UART_ClrIntEnable(x,y) { (x)->INTENCLR = (u32)(y);}


/* RXDATA - Receiver data register */

/** Get UART receive data register.
	@param x pointer to UART peripheral
	@return UART receive data register RXDATA
 */
#define UART_ReceiveRegister(x)   ((x)->RXDATA)

/** Read data from UART receive data register.
	@param x pointer to UART peripheral
	@return byte value of receive data register RXDATA (8bit)
 */
#define UART_GetData(x)           ((u8)((x)->RXDATA))


/* RXDATSTAT - Receiver data with status register */

/** Get UART receive data with status register.
	@param x pointer to UART peripheral
	@return UART receive data with status register RXDATSTAT
 */
#define UART_ReceiveDataStatRegister(x)   ((x)->RXDATSTAT)

/** Read data and status from UART receive data with status register.
	@param x pointer to UART peripheral
	@return value of receive data with status register RXDATSTAT (16bit)
 */
#define UART_GetDataStat(x)           ((u16)((x)->RXDATSTAT))


/* TXDATA - Transmit Data Register */

/** Get UART transmit data register.
	@param x pointer to UART peripheral
	@return UART transmit data register TXDATA
 */
#define UART_TransmitRegister(x)  ((x)->TXDATA)

/** Write data to UART transmit data register.
	@param x pointer to UART peripheral
	@param d value to write
	@return UART transmit data register TXDATA
 */
#define UART_SetData(x, d)        {(x)->TXDATA = (u32)((d)&0x1ff);}


/* BRG */

/** Set new divisor by writing to Baud Rate Generator register (BRG).
	@param x pointer to UART peripheral
	@param y 16bit divisor value to write (1..65536)
 */
#define UART_SetDivider(x,y)      {(x)->BRG = ((y)-1)&0xffff;}

/* INTSTAT */

#define UART_INTSTAT_RXRDY        (1UL << 0)     ///! Receiver ready interrupt
#define UART_INTSTAT_TXRDY        (1UL << 2)     ///! Transmitter ready interrupt
#define UART_INTSTAT_TXIDLE       (1UL << 3)     ///! Transmitter idle interrupt
#define UART_INTSTAT_DELTACTS     (1UL << 5)     ///! Change in CTS state interrupt
#define UART_INTSTAT_TXDIS        (1UL << 6)     ///! Transmitter disable interrupt
#define UART_INTSTAT_OVERRUN      (1UL << 8)     ///! Overrun error interrupt
#define UART_INTSTAT_DELTARXBRK   (1UL << 11)    ///! Change in receiver break detection interrupt
#define UART_INTSTAT_START        (1UL << 12)    ///! Start detect interrupt
#define UART_INTSTAT_FRAMERR      (1UL << 13)    ///! Frame error interrupt
#define UART_INTSTAT_PARITYERR    (1UL << 14)    ///! Parity error interrupt
#define UART_INTSTAT_RXNOISE      (1UL << 15)    ///! Received noise interrupt
#define UART_INTSTAT_ABERR        (1UL << 16)    ///! Auto baudrate error interrupt

/** Get UART interrupt status register.
	@param x pointer to UART peripheral
	@return UART interrupt status register INTSTAT (see UART_INTSTAT_xxx macros)
 */
#define UART_GetIntStatus(x)   ( (x)->INTSTAT )


 /* Fractional Divider */
#define UART_FRGCTRL_DIV(x)      ((x)&0xFF)               ///! baudrate generation prescaler divisor (always use 255)
#define UART_FRGCTRL_MULT(x)     ((((u32)(x))<<8)&0xFF00) ///! baudrate prescaler multiplier value

/** Set fractional divider.
	@param y value to write to fractional divider register (construct from  UART_FRGCTRL_DIV and UART_FRGCTRL_MULT)
 */
#define UART_SetFractDivider(y) { LPC_SYSCTL->FRGCTRL = (u32)(y);}


/* Typedefs */

/** UART device configuration structure */
typedef struct {
	LPC_UART_Type* device;                                 ///! Pointer to phyical UART device
	void (*irq_callback)(void);                            ///! Callback pointer to call at UART interrupt
	u32 line_ctrl;                                         ///! Setup for line control register
} uart_config_t;

extern const uart_config_t uart_config_ptr[];

/* Prototypes */
extern u32  UART_SetBaudRate(u8 ch, u32 baudrate);
extern void UART_Setup(u8 ch);
extern u8   UART_Transmit(u8 ch, u8* tx_buf, u16 tx_size);
extern u8   UART_IsTransmitFinished(u8 ch);
extern u16  UART_Receive(u8 ch, u8* rx_buf, u16 rx_size, u16 scan_char);
extern LPC_UART_Type* UART_GetDevice(u8 ch);
extern u16  UART_GetReceiveCount(u8 ch);
extern u8   UART_Peek(u8 ch, u8 ofs);
#endif

