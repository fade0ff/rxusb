/** ADC Library
	Definitions for calling IAP ROM functions
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef ADC_CNF_H
#define ADC_CNF_H


#define ADC_SAMPLE_FREQ    48000 // 480000
#define ADC_LOW_POWER_MODE 1
#define ADC_ASYNC_MODE     0
#define ADC_VRANGE         ADC_TRIM_VRANGE_HIGH

/* ADC channel mapping */
#define ADC_CH_V0  0
#define ADC_CH_V1  2
#define ADC_CH_V2  3
#define ADC_CH_V3  5
#define ADC_CH_V4  6
#define ADC_CH_V5  7

#define ADC0_CHANNELS (1UL<<ADC_CH_V0)|(1UL<<ADC_CH_V1)|(1UL<<ADC_CH_V2)|(1UL<<ADC_CH_V3)|(1UL<<ADC_CH_V4)|(1UL<<ADC_CH_V5)

#endif
